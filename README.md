# Coloring Pyxels

This is a fun side-project attempting to recreate the ["Coloting Pixels" video game](https://store.steampowered.com/app/897330/Coloring_Pixels/) but in Python.
And also, adding a functionality to use your own images to create levels.
