import os
from tkinter import *
from tkinter import ttk


def get_resolutions_available(win: Tk):
    with open("./assets/misc/screen_resolutions.txt", "r") as resolutions_file:
        all_resolutions: list[str] = resolutions_file.readlines()
    ret = []
    last_int_x = 0

    for resolution in all_resolutions:
        resolution = resolution.removesuffix("\n")
        x, y = resolution.split("x")
        int_x, int_y = int(x), int(y)
        if (
            int_x >= last_int_x
            and 320 <= int_x <= win.winfo_screenwidth()
            and 200 <= int_y <= win.winfo_screenheight()
        ):
            ret.append(resolution)
            last_int_x = int_x

    if not ret:
        raise RuntimeError(
            "No screen resolution available. The game cannot be played at your current screen size."
        )
    return ret


def update_mb_text(mb: Menubutton, key: str):
    global VARS

    def wrapped(var_name, *args):
        var = VARS[key]
        mb.configure(text=var.get())

    return wrapped


VARS = {}

# Main window
root = Tk()
root.title("Coloring Pyxels - Configuration")
root.resizable(False, False)

# Main Frame object
mainframe = ttk.Frame(root, padding="3 3 12 12")
mainframe.grid(column=0, row=0, sticky=(N, W, E, S))
root.columnconfigure(0, weight=1)
root.rowconfigure(0, weight=1)

# Image banner
config_banner_frame = ttk.Frame(mainframe, padding="2 2 2 2")
config_banner_frame.pack()
config_banner_canvas = Canvas(config_banner_frame, width=432, height=163)
config_banner_canvas.pack()
config_banner_img = PhotoImage(
    file=os.path.join("assets", "images", "coloring-pyxels-logo.png")
)
config_banner_canvas.create_image(0, 0, anchor=NW, image=config_banner_img)

# Config tabs
tab_control = ttk.Notebook(mainframe)
tab_graphics = ttk.Frame(tab_control)
tab_graphics.grid(column=0, row=0, sticky=(N, W, E, S))
tab_input = ttk.Frame(tab_control)
tab_input.grid(column=0, row=0, sticky=(N, W, E, S))
tab_control.add(tab_graphics, text="Graphics")
tab_control.add(tab_input, text="Input")
tab_control.pack(expand=1, fill="both")


# Graphics tab content
# resolution
resolutions = get_resolutions_available(root)
label_resolution = ttk.Label(tab_graphics, text="Screen resolution")
label_resolution.grid(row=0, column=0)
mb_resolution = ttk.Menubutton(tab_graphics, text=resolutions[-5])
resolutions_var = StringVar(value=resolutions[-5])
VARS["resolution"] = resolutions_var
resolutions_var.trace("w", update_mb_text(mb_resolution, "resolution"))
mb_resolution.menu = Menu(mb_resolution, tearoff=0)
mb_resolution["menu"] = mb_resolution.menu
for idx, resolution in enumerate(resolutions):
    mb_resolution.menu.add_radiobutton(label=resolution, variable=resolutions_var)
mb_resolution.grid(row=0, column=1)
windowed_var = BooleanVar(value=True)
VARS["windowed"] = windowed_var
checkbox_windowed = ttk.Checkbutton(
    tab_graphics, text="Windowed", variable=windowed_var
)
checkbox_windowed.grid(row=0, column=2)

# quality
label_quality = ttk.Label(tab_graphics, text="Graphics quality")
label_quality.grid(row=1, column=0)
mb_quality = ttk.Menubutton(tab_graphics, text="VSync On")
vsync_var = StringVar(value="VSync On")
VARS["vsync"] = vsync_var
vsync_var.trace("w", update_mb_text(mb_quality, "vsync"))
mb_quality.menu = Menu(mb_quality, tearoff=0)
mb_quality["menu"] = mb_quality.menu
mb_quality.menu.add_radiobutton(label="VSync On", value="VSync On", variable=vsync_var)
mb_quality.menu.add_radiobutton(
    label="VSync Off", value="VSync Off", variable=vsync_var
)
mb_quality.grid(row=1, column=1)


# Input tab content
# Maybe one day haha


def start_game(*args):
    from src.coloring_pyxels import start

    x, y = resolutions_var.get().split("x")
    res_x, res_y = int(x), int(y)
    windowed = windowed_var.get()
    vsync = True if "On" in vsync_var.get() else False
    root.destroy()
    start((res_x, res_y), windowed, vsync)


# Play and exit buttons
play_btn = ttk.Button(mainframe, text="Play!", command=start_game)
play_btn.bind("<Return>", start_game)
play_btn.focus()
play_btn.pack()
exit_btn = ttk.Button(mainframe, text="Exit", command=lambda: exit(0))
exit_btn.pack()

# Main loop
mainloop()
