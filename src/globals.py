import pygame

from .scenes import ScenesEnum


class Globals:
    CURRENT_SCENE: ScenesEnum = ScenesEnum.MAIN_MENU

    def change_current_scene(self, new_scene_number: ScenesEnum):
        self.CURRENT_SCENE = new_scene_number

    def change_current_scene_action(self, new_scene_number: ScenesEnum):
        def wrapped():
            self.change_current_scene(new_scene_number)

        return wrapped

    @staticmethod
    def stop_game():
        pygame.quit()
        exit()


globals = Globals()

ALLOWED_CHARS = [
    " ",
    "(",
    ")",
    "+",
    ",",
    "-",
    ".",
    "0",
    "1",
    "2",
    "3",
    "4",
    "5",
    "6",
    "7",
    "8",
    "9",
    "A",
    "B",
    "C",
    "D",
    "E",
    "F",
    "G",
    "H",
    "I",
    "J",
    "K",
    "L",
    "M",
    "N",
    "O",
    "P",
    "Q",
    "R",
    "S",
    "T",
    "U",
    "V",
    "W",
    "X",
    "Y",
    "Z",
    "[",
    "]",
    "_",
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
    "~",
]
