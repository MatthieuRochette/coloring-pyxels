import pygame

from . import ScenesEnum
from .level_creator import LevelCreator
from .level_scene import LevelScene
from .main_menu import MainMenu
from .scene import Scene
from .settings import Settings

available_scene_types: dict[ScenesEnum, Scene] = {
    ScenesEnum.MAIN_MENU: MainMenu,
    ScenesEnum.LEVEL: LevelScene,
    ScenesEnum.LEVEL_CREATOR: LevelCreator,
    ScenesEnum.SETTINGS: Settings,
}
active_scenes = {}


def update_scene(
    scene_num: ScenesEnum, events: list[pygame.event.Event], display: pygame.Surface
):
    scene: Scene = active_scenes.get(scene_num, None)
    if scene is None:
        active_scenes[scene_num] = available_scene_types[scene_num](parent=display)
        scene = active_scenes[scene_num]
    scene.update(events)


def render_scene(scene_num: ScenesEnum, display: pygame.Surface):
    scene: Scene = active_scenes.get(scene_num, None)
    if scene is None:
        active_scenes[scene_num] = available_scene_types[scene_num](parent=display)
        scene = active_scenes[scene_num]
    scene.render()
