import os

import pygame
import pygame_menu
from pygame_menu import themes

from src.scenes import ScenesEnum

from ..globals import globals
from ..maps import get_maps_names
from .scene import Scene


class MainMenu(Scene):
    def _setup_left_menu(self):
        self.left_menu = pygame_menu.Menu(
            title="",
            width=self.logo.get_width(),
            height=self.parent.get_height() - self.logo.get_height(),
            theme=themes.THEME_ORANGE,
        )
        self.left_menu.set_absolute_position(0, self.logo.get_height())
        self.left_menu.add.button(
            title="Level Creator",
            action=globals.change_current_scene_action(ScenesEnum.LEVEL_CREATOR),
        )
        self.left_menu.add.button(
            title="Settings",
            action=globals.change_current_scene_action(ScenesEnum.SETTINGS),
        )
        self.left_menu.add.button(title="Exit", action=globals.stop_game)

    def _select_level_action(self):
        def wrapped(selector, value):
            self.chosen_level = value

        return wrapped

    def _setup_level_menu(self):
        self.lvl_menu = pygame_menu.Menu(
            title="Choose a level:",
            width=self.parent.get_width() - self.logo.get_width(),
            height=self.parent.get_height(),
            theme=themes.THEME_BLUE,
        )
        self.lvl_menu.set_absolute_position(self.logo.get_width(), 0)
        # Need to have selector values as key/value pairs
        maps = [(map, map) for map in get_maps_names()]
        self.lvl_menu.add.selector(
            title="Level name:",
            items=maps,
            onchange=self._select_level_action(),
        )

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.chosen_level: str = ""
        self.logo = pygame.image.load(
            os.path.join("assets", "images", "coloring-pyxels-logo.png")
        )
        self._setup_left_menu()
        self._setup_level_menu()

    def update(self, events: list[pygame.event.Event]):
        self.left_menu.update(events)
        self.lvl_menu.update(events)

    def render(self) -> None:
        self.parent.blit(source=self.logo, dest=(0, 0))
        self.lvl_menu.draw(self.parent)
        self.left_menu.draw(self.parent)
