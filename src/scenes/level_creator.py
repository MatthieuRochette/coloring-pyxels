from tkinter.filedialog import askopenfilename

import pygame
import pygame_menu
from pygame_menu import themes
from pygame_menu.widgets.widget.textinput import TextInput
from pygame_menu.widgets.widget.progressbar import ProgressBar
from pygame_menu.widgets.widget.rangeslider import RangeSlider
from pygame_menu.widgets.widget.button import Button

from ..globals import ALLOWED_CHARS, globals
from ..maps.map_creator import create_map
from ..maps.map_creator_params import MapCreatorParams
from .scene import Scene
from .scene_manager import ScenesEnum


class LevelCreator(Scene):
    def ask_file_input_action(self):
        def wrapped():
            self.origin_image_path = askopenfilename()
            self.file_button.set_title(
                f"Choose an image as the origin (chosen: {self.origin_image_path})"
            )

        return wrapped

    def create_new_map_action(self):
        def wrapped():
            if not self.origin_image_path:
                print("ERROR: cannot create map without selecting a file!")
                return

            self.current_menu = self.progress_menu
            self.map_params = MapCreatorParams(
                map_name=self.map_name_field.get_value(),
                origin_image_path=self.origin_image_path,
                wanted_max_colors=int(self.max_colors_slider.get_value()),
                wanted_resolution=(
                    int(self.res_x_field.get_value()),
                    int(self.res_y_field.get_value()),
                ),
            )
            created_map = create_map(self.map_params, self.progress_bar)
            globals.change_current_scene(ScenesEnum.MAIN_MENU)

        return wrapped

    def setup_params_menu(self):
        self.params_menu = pygame_menu.Menu(
            title="Level creator",
            height=self.parent.get_height(),
            width=self.parent.get_width(),
            position=(0, 0, True),
            theme=themes.THEME_GREEN,
        )
        self.map_name_field: TextInput = self.params_menu.add.text_input(
            title="Choose a name: ",
            default="my fun new map",
            maxchar=80,
            valid_chars=ALLOWED_CHARS,
        )
        self.file_button: Button = self.params_menu.add.button(
            title="Choose an image as the origin", action=self.ask_file_input_action()
        )
        self.max_colors_slider: RangeSlider = self.params_menu.add.range_slider(
            title="Max number of colors: ",
            default=25,
            increment=1,
            range_values=(1, 100),
            width=300,
        )
        self.params_menu.add.label(title="Resolution of the new map:")
        self.res_x_field: TextInput = self.params_menu.add.text_input(
            title="x=",
            valid_chars=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            copy_paste_enable=False,
            default="100",
        )
        self.res_y_field: TextInput = self.params_menu.add.text_input(
            title="y=",
            valid_chars=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
            default="100",
        )
        self.params_menu.add.button(
            title="Create map !", action=self.create_new_map_action()
        )

    def setup_progress_menu(self):
        self.progress_menu = pygame_menu.Menu(
            title="Level creator",
            height=self.parent.get_height(),
            width=self.parent.get_width(),
            position=(0, 0, True),
            theme=themes.THEME_GREEN,
            mouse_enabled=False,
            joystick_enabled=False,
            keyboard_enabled=False,
        )
        self.progress_bar: ProgressBar = self.progress_menu.add.progress_bar(
            title="Creating map...", width=500
        )

    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.origin_image_path: str = ""
        self.map_params: MapCreatorParams = None
        self.setup_params_menu()
        self.setup_progress_menu()
        self.current_menu = self.params_menu

    def update(self, events: list[pygame.event.Event]):
        self.current_menu.update(events)

    def render(self):
        self.current_menu.draw(self.parent)
