from abc import ABC

import pygame


class Scene(ABC):
    def __init__(self, parent: pygame.Surface):
        self.parent = parent

    def update(self, events: list[pygame.event.Event]):
        raise NotImplementedError("This scene is not developed yet.")

    def render(self):
        raise NotImplementedError("This scene is not developed yet.")
