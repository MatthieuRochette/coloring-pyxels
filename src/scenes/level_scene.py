import pygame

from .scene import Scene


class LevelScene(Scene):
    def __init__(self, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)

    def update(self, events: list[pygame.event.Event]):
        return

    def render(self):
        return
