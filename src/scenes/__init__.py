from enum import Enum


class ScenesEnum(Enum):
    MAIN_MENU = 0
    LEVEL = 1
    LEVEL_CREATOR = 2
    SETTINGS = 3
