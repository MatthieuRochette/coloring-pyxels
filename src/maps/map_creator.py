import datetime
import os
from PIL import Image
import shutil


from pygame_menu.widgets.widget.progressbar import ProgressBar

from src.maps.map import Map
from src.maps.map_creator_params import MapCreatorParams


def create_map(params: MapCreatorParams, progress_bar: ProgressBar) -> Map:
    print(params),
    """
    Ouvrir l'image

    Resize l'image à la résolution cible

    Réduire le nombre de couleurs dans l'image au nombre max (ou moins)

    Créer une classe de Map() à partir de l'image

    Sauvegarder dans un format pickled
    """
    map_name = params.map_name.replace(" ", "&sp;")
    map_path = (
        os.path.join("assets", "images", "maps_sources", map_name)
        + "."
        + params.origin_image_path.split(".")[-1]
    )
    progress_bar.set_value(progress_bar.get_value() + 5)
    shutil.copyfile(
        params.origin_image_path,
        map_path,
    )
    progress_bar.set_value(progress_bar.get_value() + 5)
    img = Image.open(map_path)
    progress_bar.set_value(progress_bar.get_value() + 5)
    img = img.resize(size=(params.wanted_resolution))
    progress_bar.set_value(progress_bar.get_value() + 10)
    converted = img.convert(
        "P", palette=Image.ADAPTIVE, colors=params.wanted_max_colors
    )
    progress_bar.set_value(progress_bar.get_value() + 50)
    now = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S")
    converted.save(f"assets/images/output_test/latest-{map_name}-{now}.png")
    progress_bar.set_value(100)
