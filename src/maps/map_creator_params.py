from dataclasses import dataclass


@dataclass
class MapCreatorParams:
    map_name: str
    origin_image_path: str
    wanted_resolution: tuple[int, int]
    wanted_max_colors: int
