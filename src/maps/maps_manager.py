import os


def get_maps_names():
    return os.listdir(os.path.join("assets", "maps"))
