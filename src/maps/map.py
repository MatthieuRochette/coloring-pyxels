from dataclasses import dataclass


@dataclass
class Color:
    id: int
    r: int
    g: int
    b: int

    def get_rgb(self):
        return (self.r, self.g, self.b)


@dataclass
class Pixel:
    x: int
    goal_color_id: int
    current_color_id: int


@dataclass
class Map:
    final_pixels_arr: list[Pixel]
    colors_arr: list[Color]
