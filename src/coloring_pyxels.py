# Example file showing a basic pygame "game loop"
import pygame

from .globals import globals
from .scenes.scene_manager import render_scene, update_scene


def start(resolution: tuple[int, int], windowed: bool, vsync: bool):
    global globals
    print(f"resolution={resolution}, windowed={windowed}, vsync={vsync}")
    # pygame setup
    pygame.init()
    screen = pygame.display.set_mode(resolution, vsync=vsync)
    if windowed:
        pygame.display.set_caption("Coloring Pyxels")
    else:
        print("Setting as fullscreen")
        pygame.display.toggle_fullscreen()

    running = True
    clock = pygame.time.Clock()

    while running:
        # poll for events
        # pygame.QUIT event means the user clicked X to close your window
        events = pygame.event.get()
        for event in events:
            if event.type == pygame.QUIT:
                running = False
        update_scene(globals.CURRENT_SCENE, events, screen)

        # fill the screen with a color to wipe away anything from last frame
        screen.fill("blue")

        # RENDER YOUR GAME HERE
        render_scene(globals.CURRENT_SCENE, screen)

        # flip() the display to put your work on screen
        pygame.display.flip()

        clock.tick(144)  # limits FPS to 144

    pygame.quit()
